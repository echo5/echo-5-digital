import Vuex from 'vuex'

const store = () => {
  return new Vuex.Store({
    state: {
      article: null,
      comments: [],
      replyToComment: null
    },
    mutations: {
      setArticle (state, article) {
        state.article = article
      },
      setComments (state, comments) {
        state.comments = comments
      },
      addComment (state, comment) {
        state.comments.push(comment)
      },
      setReplyToComment (state, comment) {
        state.replyToComment = comment
      },
      setAuthor (state, author) {
        state.author = author
      }
    }
  })
}

export default store
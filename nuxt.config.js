const axios = require('axios')
require('dotenv').config()
var apiUrl = process.env.apiUrl || 'https://content.echo5digital.com/wp-json/wp/v2'

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'echo5',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Echo 5 Website' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  modules: [
    ['@nuxtjs/google-tag-manager', { id: 'GTM-KZDC5K8' }],
    ['@nuxtjs/toast'],
    ['@nuxtjs/sitemap']
  ],
  plugins: [
    { src: '~/plugins/bus', ssr: false },
    { src: '~/plugins/siema', ssr: false },
    { src: '~/plugins/comments', ssr: true },
  ],
  env: {
    apiUrl: apiUrl
  },
  sitemap: {
    path: '/sitemap.xml',
    hostname: 'https://echo5web.com',
    cacheTime: 1000 * 60 * 15,
    generate: true, // Enable me when using nuxt generate
    exclude: [
      '/contact-old'
    ],
    routes () {
      return axios.all([
        axios.get(`${apiUrl}/posts?per_page=100`),
        axios.get(`${apiUrl}/work?per_page=100`),
        axios.get(`${apiUrl}/services?per_page=100`),
      ])
      .then(axios.spread((articles, work, services) => [
        ...services.data.map(service => '/services/' + service.slug),
        ...articles.data.map(article => '/blog/' + article.slug),
        ...work.data.map(item => '/work/' + item.slug),
      ]))
    }
  }
}

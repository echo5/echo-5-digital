import Vue from 'vue'
import Comment from '~/components/Comment'
import CommentList from '~/components/CommentList'

Vue.component('Comment', Comment)
Vue.component('CommentList', CommentList)